module.exports = function (server) {
  const { check, validationResult } = require('express-validator')
  require('../services/userService')(server)
  server.post('/checkAPI', [
    check('name').isLength({ min: 3 }),
    check('email').isEmail(),
    check('age').isNumeric()
  ], (request, response) => {
    const errors = validationResult(request)
    if (!errors.isEmpty()) {
      var errorArray = errors.array()
      var errorResponse = {}
      errorResponse.error = true
      errorResponse.message = errorArray[0].msg
      return response.send(errorResponse)
    }
    this.checkAPI(request.body, function (results) {
        return response.send(results)
    })
  })

  server.post('/getuser', server.auth, [
    check('page').isNumeric()
  ], (request, response) => {
    const errors = validationResult(request)
    if (!errors.isEmpty()) {
      var errorArray = errors.array()
      var errorResponse = {}
      errorResponse.error = true
      errorResponse.message = errorArray[0].msg
      return response.send(errorResponse)
    }
    console.log(request.body)
  })
}
