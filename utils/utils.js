module.exports = function (server) {
  var jwt = require('jsonwebtoken')
  this.generateToken = function (data) {
    return new Promise(function (resolve) {
      jwt.sign(data, process.env.JWT_SECRET_KEY, (err, token) => {
        if (err) {
          resolve(err)
        } else {
          resolve(token)
        }
      })
    })
  }

  this.tokenVerification = async (token) => {
    var data = {}
    return new Promise(function (resolve) {
      jwt.verify(token, process.env.JWT_SECRET_KEY, (err, payload) => {
        if (err) {
          data.error = true
          data.data = null
          resolve(data)
        } else {
          data.error = false
          data.data = payload
          resolve(data)
        }
      })
    })
  }
}
