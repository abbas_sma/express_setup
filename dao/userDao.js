module.exports = function (server) {
  this.checkAPIService = function (data) {
    var response = {}
    return new Promise(function (resolve) {
      server.db('users')
        .select('id', 'mobile', 'email')
      // .where('email', email)
      // .where('hasUserCompletedOTPVerification', 1)
        .then((result) => {
          response.error = false
          response.users = result
        })
        .catch((error) => {
          response.error = true
        })
        .finally(() => {
          resolve(response)
        })
    })
  }
}
