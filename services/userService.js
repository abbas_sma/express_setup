module.exports = function (server) {
  require('../dao/userDao')(server)
  require('../utils/utils')(server)
  this.checkAPI = async (request, callback) => {
    var checkResponse = await this.checkAPIService(request)
    var data = {}
    data.Id = checkResponse.users[0].id
    data.mobile = checkResponse.users[0].mobile
    var token = await this.generateToken(data)
    checkResponse.token = token
    callback(checkResponse)
  }
}
