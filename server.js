var express = require('express')
var app = express()
require('dotenv').config()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

var db = require('knex')({
  client: 'mysql',
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
  }
})

async function auth (request, response, next) {
  var errorResponse = {}
  try {
    var auth = await this.tokenVerification(request.headers.authorization)
    if (auth.error) {
      errorResponse.error = true
      errorResponse.message = 'Unauthorized'
      return response.send(errorResponse)
    } else {
      request.params.auth = auth.data
    }
  } catch (e) {
    errorResponse.error = true
    errorResponse.message = 'Unauthorized'
    return response.send(errorResponse)
  }
  next()
}

app.auth = auth
require('./routes/routes')(app)

app.db = db
var server = app.listen(8000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('Example app listening at http://%s:%s', host, port)
})
